#!/usr/bin/env python
# -*- encoding: utf8 -*-

#导入模块，urllib2是一个模拟浏览器HTTP方法的模块
import json
import urllib2
import sys
from urllib2 import Request,urlopen,URLError,HTTPError
from auth import zabbix_header,zabbix_pass,zabbix_url,zabbix_user,auth_code,MySQLport,MySQLuser,MySQLpasswd
import MySQLdb
#request json
json_data ={
    "jsonrpc": "2.0", #版本
    "method": "hostgroup.get",    #接口方法名（主机组操作）
    "params": {                     #接口参数
        "output": "extend",
        "filter": {
            "name": [
            ]
        }
    },
    "auth": auth_code,               #将登录中正确的密钥放入auth
    "id": 1                          #任意数
}

#用得到的SESSIONID去验证，获取主机的信息(用http.get方法)

#如果 auth_code的长度为0  就报错 #如果不等于0  将json_data变量的数据json格式化
if len(auth_code) == 0:
        sys.exit(1)
if len(auth_code) != 0:
        get_host_data = json.dumps(json_data)#将我的json_data变为jason格式

        #create request object
        #通过urllib2发送http请求 循环读取zabbix_header中的变量
        request = urllib2.Request(zabbix_url,get_host_data)
        for key in zabbix_header:
                request.add_header(key,zabbix_header[key])

        #get host list
        try:
                result = urllib2.urlopen(request)#根据三个参数发送请求
        except URLError as e:
                if hasattr(e,'reason'):
                        print 'We failed to reach a server.'
                        print 'Reason: ',e.reason
                elif hasattr(e,'code'):
                        print 'The server could not fulfill the request.'
                        print 'Error code: ',e.code
        else:
                response = json.loads(result.read())#读取所有返回值
                result.close()#将请求关闭
#-----------下边是将得到的数据插入数据库-------

                #所有json数据，然后对其进行提取！

                #统计几个groups

                # 连接数据库
                conn = MySQLdb.connect(#给这个函数传参数
                    host='localhost',
                    port=MySQLport,
                    user=MySQLuser,
                    passwd=MySQLpasswd,
                    db='cmc',
                    charset='utf8',
                )
                cur = conn.cursor()#连接
                # 清空数据库
                sql1 = "DELETE FROM main_chostgroups "#删除main_chostgroups这个表格
                cur.execute(sql1)#执行70行
                conn.commit()#提交
                #显示主机名称，并循环读取将每个主机定义一个变量
                for host in response['result']:
                        get_data =[host['groupid'], host['name'] ]
                        #此处注意！！要加一个括号[]！！！
                        # print get_data
                        # 连接数据库mysql
                        #cur = conn.cursor()

                        # 插入一条数据
                        sql2 = "insert into main_chostgroups values(%s,%s) "
                        try:
                            cur.execute(sql2, get_data)  # 执行sql语句
                            conn.commit()
                            #print "insert success!"
                        except:
                            print "Error: unable to fetch data"

                cur.close()
                conn.close()