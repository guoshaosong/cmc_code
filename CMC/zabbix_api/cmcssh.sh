#!/bin/bash
export name=root
export pass=111111
export to_command=$1

expect -c "
    spawn $to_command
    expect {
        \"*assword\" {set timeout -1; send \"$pass\r\"; exp_continue;}
        \"yes/no\" {send \"yes\r\";}
    }
expect eof"
