# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.shortcuts import HttpResponse,render,redirect
from django.http import HttpResponseRedirect
from models import Names
import socket
import hashlib
import os


#获取本地地址
def get_host_ip():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()
   # ip = 'hao.cross.echosite.cn'
    return ip


#注册
def do_regist(request):
    myhostip = get_host_ip()
    if request.method == 'GET':
        # 当请求为get请求的的时候，跳转到register.html.
        return render(request, 'login/register1.html',{'myhostip':myhostip})
    if request.method == 'POST':
            # 获取表单数据
            uname = request.POST.get('username')
            pwd = request.POST.get('pwd')

            #加密密码
            md5 = hashlib.md5()
            md5.update(pwd)
            repwd=md5.hexdigest() #获取加密文本，并把文本复制给变量名repwd
            #创建对象（插入数据库）
            Names.objects.create(username=uname, password=repwd)
            #创建目录
            os.makedirs('/etc/zabbix/script/%s/item'%(uname))
            os.makedirs('/etc/zabbix/script/%s/action' % (uname))
            return render(request, 'login/login1.html', {'username': uname, 'myhostip': myhostip})



# login方法处理登录
def do_login(request):
    myhostip = get_host_ip()

    # 处理get请求
    if request.method == 'GET':
        # 当请求为get请求的的时候，跳转到login.html.
        return render(request,'login/login1.html',{'myhostip':myhostip})
    else:
        # 处理用户发来的post请求
            # 获取表单数据
        uname = request.POST.get('username')
        pwd = request.POST.get('pwd')
        #对密码进行加密
        md5 = hashlib.md5()
        md5.update(pwd)
        repwd = md5.hexdigest()  #获取加密文本，并把文本复制给变量名repwd
        # 获取的表单数据与数据库进行比较
        user = Names.objects.filter(username=uname, password=repwd)#后两个是表单数据的，

        if user:
    #给session域中存放键值对｛‘username’：uname｝,username为前端获取的参数名，值为uname，也就是传递的数据
            request.session['username'] = uname
           # 比较成功，跳转主页面
            return HttpResponseRedirect('/login/login_save_data/')
        else:
            # 登录用户名或密码错误
            return render(request,'login/login1.html',{'msg':'password or username is wrong!'})
def login_save_data(request):
    myhostip = get_host_ip()
    username = request.session.get('username')  #session获取参数名叫username的值
    return render(request,'main/main.html',{'username':username,'myhostip':myhostip})

#安全退出
def logout(request):
    del request.session['username']
    return HttpResponseRedirect('/login/do_login/')