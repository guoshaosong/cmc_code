#encoding:utf-8
from __future__ import unicode_literals

from django.db import models

class Names(models.Model):
    username = models.CharField(max_length=128)
    password = models.CharField(max_length=128)
    def __unicode__(self ):
        return self.username
